from functools import reduce


class Polygon:
    sides = None

    def __init__(self, sides):
        self.sides = sides

    @property
    def half_perimeter(self) -> float:
        return 0.5 * sum(self.sides)


class Triangle(Polygon):
    @property
    def square(self):
        if self.is_triangle:
            p = self.half_perimeter
            return pow((p * reduce((lambda a, b: a * (p - b)), self.sides)), 0.5)
        else:
            return None

    @property
    def is_triangle(self):
        side = self.sides
        try:
            assert (len(self.sides) == 3), "A Triangle should have 3 sides"
            for i in range(3):
                assert (side[i % 3] + side[(i + 1) % 3]) > side[(i + 2) % 3], \
                    "The sum of two sides is smaller than or equals the third one"
        except AssertionError as error:
            print(error)
            return False
        else:
            return True


class ConsoleDialog:

    @staticmethod
    def read_answer(question):
        #print(question)
        return input(question)

    @staticmethod
    def read_string_list_answer(question):
        return ConsoleDialog.read_answer(question).strip().split(" ")

    @staticmethod
    def read_float_list_answer(question):
        string_list = ConsoleDialog.read_string_list_answer(question)
        float_list = []
        try:
            for string in string_list:
                float_list.append(float(string))
        except ValueError:
            print("You've entered some unsupported symbols:", string_list)
        return float_list


if __name__ == '__main__':
    polygon_sides = ConsoleDialog.read_float_list_answer("Input polygon sides lengths using space separator:\n")
    if Triangle(polygon_sides).square:
        print("Triangle square = ", '%.2f' % Triangle(polygon_sides).square)
