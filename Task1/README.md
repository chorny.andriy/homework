# task 1
I've built Task1 Project using 2 classes:
- Polygon;
- Triangle, which inherits Polygon.

Polygon class has:
- a List of sides (side lenths) as its data 
- a @property method half_perimeter() for further calculations;

Triangle inherits Polygon and obtains new @property methods:
- istriangle() property method checks if the polygon is a triangle according to number of sides 
and an axiom that the sum of any two sides of a triangle is larger than the third side;
- square() property method calculates an area of a the triangle if it is a triangle using Heron's formula.

15.10.2020
Implemented console input of triangle side lenths input



