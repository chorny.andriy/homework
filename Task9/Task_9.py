import string
import re


class String:
    """
    Class String implements static methods for strings transformation according to Task 9
    """

# Exercise 1
    @staticmethod
    def replace_all_quotes(string_arg: str) -> str:
        """
        Replaces all double quotes with single quotes and vise versa, splitting a string by
        single quotes, than replacing in substring double quotes with single quotes and than joining substrings with
        double quotes, using list comprehension.
        :param string_arg: str
        :return: str
        """
        sub_phrases = string_arg.split("'")
        return '"'.join(sub_phrase.replace('"', "'") for sub_phrase in sub_phrases)

    @staticmethod
    def get_words(string_arg: str) -> list:
        """
        Returns list of words in thr string using Regular Expression.
        :param string_arg: str
        :return: list
        """
        return re.findall(r"\b[a-zA-Z]+\b", string_arg)

# Exercise 2
    @staticmethod
    def is_palindrome(string_arg: str) -> bool:
        """
        Returns True if given string is a palindrome.
        :param string_arg: str
        :return: bool
        """
        # normalized - is a transformed string containing only alphabetic symbols in lower case.
        normalized = ''.join(String.get_words(string_arg)).lower()
        return normalized.find(normalized[::-1]) != -1

# Exercise 3
    @staticmethod
    def get_shortest_word(string_arg: str) -> str:
        """
        Returns the first of the shortest words from the given string.
        :param string_arg: str
        :return: str
        """
        return min([(len(word), index, word) for index, word in enumerate(String.get_words(string_arg), 0)])[2]

# Exercise 4
    @staticmethod
    def _characters_appeared_in_strings_by_condition(conditional_function, *string_args: str) -> list:
        """
        Returns a list of characters, which appear in given strings by given conditions
        :param conditional_function: returns bool according to inner conditional operations; as an argument takes a list
        of booleans, which represent an appearance of the character in a string from *string_args.
        :param string_args: a changeable number of strings.
        :return: a list of characters, which appear in given strings according to conditions of conditional_function.
        """
        character_set = []
        for char in string.ascii_lowercase:
            if conditional_function([string_arg.lower().find(char) != -1 for string_arg in string_args]):
                character_set.append(char)
        return character_set

    @staticmethod
    def character_appeared_in_all(*string_args: str) -> list:
        return String._characters_appeared_in_strings_by_condition(
            lambda appearance_list: appearance_list.count(True) == len(string_args), *string_args)

    @staticmethod
    def character_appeared_at_least_in_one(*string_args: str) -> list:
        return String._characters_appeared_in_strings_by_condition(
            lambda appearance_list: True in appearance_list, *string_args)

    @staticmethod
    def character_appeared_at_least_in_two(*string_args: str) -> list:
        return String._characters_appeared_in_strings_by_condition(
            lambda appearance_list: appearance_list.count(True) >= 2, *string_args)

    @staticmethod
    def character_appeared_in_not_any(*string_args: str) -> list:
        return String._characters_appeared_in_strings_by_condition(
            lambda appearance_list: True not in appearance_list, *string_args)

# Exercise 5
    @staticmethod
    def build_letter_occurrence_dict(string_arg) -> dict:
        return dict([(char, string_arg.count(char)) for char in string_arg.lower() if char in string.ascii_letters])


if __name__ == '__main__':
    my_string1 = '  "Was it a cat I saw?"'
    print('--------------------------------------------------------------------------')
    my_string = ' "Go Hang a salami I\'m a lasagna hog"'
    my_string2 = '  "Madam, I\'m Adam."'
    print('We have a palindrome sentence: ', my_string)
    print('--------------------------------------------------------------------------------------------------------------')
    print(f'Let\'s get words from the sentence using regex: ')
    print(String.get_words(my_string))
    print('--------------------------------------------------------------------------------------------------------------')
    print(f'Let\'s replace all " with \' and vice versa: ')
    print(String.replace_all_quotes(my_string))
    print('--------------------------------------------------------------------------------------------------------------')
    print(f'Let\'s check if our sentence is a palindrome: ')
    print(String.is_palindrome(my_string))
    print('--------------------------------------------------------------------------------------------------------------')
    print(f'Let\'s find the first of the shortest words in the sentence: ')
    print(f'\'{String.get_shortest_word(my_string)}\'')
    print('--------------------------------------------------------------------------------------------------------------')
    # my_string1 = 'hallo'
    # my_string2 = 'low'
    print(f'Let\'s get characters, which appear in every string of: "{my_string}", "{my_string1}", "{my_string2}"')
    print(String.character_appeared_in_all(my_string, my_string1, my_string2))
    print('--------------------------------------------------------------------------------------------------------------')
    print(f'Let\'s get characters, which appear at least in one string of: "{my_string}", "{my_string1}", "{my_string2}"')
    print(String.character_appeared_at_least_in_one(my_string, my_string1, my_string2))
    print('--------------------------------------------------------------------------------------------------------------')
    print(f'Let\'s get characters, which appear at least in two string of: "{my_string}", "{my_string1}", "{my_string2}"')
    print(String.character_appeared_at_least_in_two(my_string, my_string1, my_string2))
    print('--------------------------------------------------------------------------------------------------------------')
    print(f'Let\'s get characters, which appear in not any string of: "{my_string}", "{my_string1}", "{my_string2}"')
    print(String.character_appeared_in_not_any(my_string, my_string1, my_string2))
    print('--------------------------------------------------------------------------------------------------------------')
    print(f'Let\'s count characters appearence in : "{my_string}')
    print(String.build_letter_occurrence_dict(my_string))
