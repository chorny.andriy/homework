class Cell(object):
    item: object = None
    next_item = None

    def __init__(self, obj: object):
        self.item = obj
        self.next_item = None

    def __next__(self):
        if self.next_item:
            return self.next_item
        else:
            raise StopIteration

    def __str__(self):
        return self.item.__str__()

    def __get__(self, instance, owner):
        return self.item

    def has_next(self):
        return self.next_item

    def _add_cell_(self, obj):
        self.next_item = Cell(obj)

    def _next_str_(self):
        if self.has_next():
            return self.__str__() + ', ' + self.__next__()._next_str_()
        else:
            return self.__str__()


class CustomList:
    head: Cell
    def __init__(self):
        self.head = Cell(None)

    def __iter__(self) -> Cell:
        cell = self.head
        while cell is not None:
            yield cell
            cell = cell.__next__()

    def __str__(self):
        return '[' + self.head._next_str_() + ']'

    def __next__(self):
        if self.head:
            return self.head
        else:
            raise StopIteration

    def append(self, obj):
        if self.is_empty():
            self.head = Cell(obj)
        else:
            self._append_to_tale_(obj)

    def _append_to_tale_(self, obj):
        self._get_tale_()._add_cell_(obj)

    def _get_tale_(self):
        tale = self.head
        while tale.has_next():
            tale = tale.__next__()
        return tale

    def is_empty(self):
        return self.head == None

    def get(self, index) -> Cell:
        element = self.head
        for i in range(index):
            element = element.__next__()
        return element



if __name__ == '__main__':
    my_list = CustomList()
    my_list.append((1, 2, 3))
    my_list.append([1, 2, 3])
    my_list.append(256)
    my_list.append({'s': 1, 'd': 2})
    my_list.append('Andrii')
    my_list.append('Chornyi')
    my_list.append('Natalia')
    my_list.append('Chorna')
    my_list.append('are')
    my_list.append('The Best')
    print(my_list)
    print(my_list.get(3))

    for element in my_list:
        print(element)

# ---------------------------------SOME GARBAGE---------------------------------------
        # index = 0
        # for element in self:
        #     if item == element.item:
        #         return index
        #     index += 1
        # return None


        # element = self.head
        # if element:
        #     for i in range(index):
        #         element = next(element)
        # return element


        # replaced = Cell(item)
        # element = self.get(index-1)
        # replaced.next_item = next(next(element))
        # element.next_item = replaced


        # cell = self.head
        # len = 1
        # while next(cell):
        #     len += 1
        #     cell = next(cell)
        # return len


    # my_list.append((1, 2, 3))
    # my_list.append([1, 2, 3])
    # my_list.append(256)
    # my_list.append({'s': 1, 'd': 2})
    # my_list.append('Andrii')
    # my_list.append('Chornyi')
    # my_list.append('Natalia')
    # my_list.append('Chorna')
    # my_list.append('are')
    # my_list.append('The Best')
    # print(my_list)
    # #print(next(my_list.get(3)))
    # print('deleted: ', my_list.pop(3))
    # my_list.insert(3, 'it was dict')
    # my_list.replace(2, 512)
    # for element in my_list:
    #     print(element)
    # print('len:', len(my_list))
    # find_element = 'Chorna'
    # if find_element in my_list:
    #     print(f'index of element {find_element} is: ', my_list.get_index(find_element))
    # print(my_list[1])
    # my_list[0] = (1, 2, 3, 4, 5)
    # print(my_list)
    # my_list_from_list = CustomList([6, 5, 4, 3, 2, 1])
    # print(my_list_from_list)
    # print(len(my_list_from_list))


    lst = []
    lst.__getitem__()

    obj: set
    obj.__next__()

    # if not isinstance(index, int):
    #     raise TypeError(f'Wrong type of index: {type(index).__name__} = {index}')
    # elif index < 0 or index >= self.__len__():
    #     raise IndexError(f'Index out of boundaries: {index}')

    # def _get_index_of_max_area(self, attr: str):
    #     max_area_index = None
    #     for i, rectangle in enumerate(self, 0):
    #         if not max_area_index or (rectangle and rectangle.__getattribute__(attr) > max_area_index['area']):
    #             max_area_index = {'index': i, 'area': rectangle.area}
    #     if max_area_index:
    #         return max_area_index['index']
    #     return None
