# print(globals())
# print((locals()))
# help()
# class test:
#     name = "test"
#     description = "Testing class"
#     quantity = 1
#     def showInfo(self):
#         print(dir(self))
# test.showInfo(test)
# print(dir())

# a = int(input('Enter the number: '))
# b = 7
# assert a > b, 'Not enough'

# def tryreturn():
#     try:
#         return 1
#         with open('/tmp/logs.txt') as file:
#             print(file.read())
#             return
#     finally:
#         return 2
# result = tryreturn()
# print(result)

# def func():
#     try:
#         print(1)
#         return
#     else:
#         print(3)
#     finally:
#         print(2)
#
# func()

# def func():
#     try:
#         assert 1>2, 'False' # f1(x, 4)
#     finally:
#         print('finally')
#     print('after try')
# func

# try:
#     if '1' != 1:
#         raise 'Error'
#     else:
#         print('No error')
# except:
#     print('Error')

# flag = False
# while not flag:
#     try:
#         f = input('Enter f: ')
#         file = open(f, 'r')
#     except:
#         print('Input f not f')

# letters = list(map(lambda x: x, range(10)))
# print(letters)
# for x in 'human':
#     print(x)
# import os

# print(os.name)
# print(os.getcwd())
# fd = 'test.txt'
# f = open(fd, 'w')
# f.close()
# file = os.popen(fd, 'w')
# file.write('Hello, Natalia!!! Love you!!!')
# os.rename(fd, 'test1.txt')
log_stat = False
def decorator(func):
    def wrapper(*args, **kwargs):
        global log_stat
        while not log_stat:
            username = input("username")
            password = input("password")
            if username == "123" and password == "456":
                log_stat = True
                return func(*args, **kwargs)
            else:
                print("try again")  # I would raise an exception here
    return wrapper


@decorator
def welcome(*args):
    print(args)

print(welcome())