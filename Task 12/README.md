#Home Task 12

##Task 1b:

• ls -l / 		- lists the root directory contents in detailed format
• ls			- lists the contents of current directory 
• ls ~			- lists the contents of current users home directory
• ls -l			- list the contents of current directory in detailed format
• ls -a			- lists the contents of current directory including hidden files/folders starting with .
• ls -la		- the same as previous but in detailed format
• ls -lda ~		- lists detailed information of home directory itself, not its contents

##Task 1c:
• mkdir test			- creating directory test in current directory
• cd test				- changing current directory to test
• pwd					- shows current directory path
• touch test.txt		- creates empty file test.txt in current directory
• ls -l test.txt		- lists detailed information about file test.txt
• mkdir test2
• mv test.txt test2		- moves test.txt to test2 directory
• cd test2				- changing current directory to test2
• ls					- lists the contents of current directory 
• mv test.txt test2.txt - renames file test.txt to test2.txt
• ls					- lists the contents of current directory 
• cp test2.txt ..		- copies file test.txt to parent directory
• cd ..					- changes current directory to parrent
• ls					- lists the contents of current directory
• rm test2.txt			- removing file test2.txt from current directory
• rmdir test2			- removing directory test2 from current directory

##Task 1d:
• cat /etc/fstab		- just prints to standart output file contents (an information about file system and storage devices)
• less /etc/fstab		- a program for listing a text file in terminal screen, much more featured than more, is not closing automatically untill q is pressed,
							doesn't load the whole file at the start
• more /etc/fstab		- a program for listing a text file in terminal screen, loads the whole file at the start and displays it on the screen, there is 
							an 	ability for text paging in both directions
							
##Task 1e:
• man cat 	- Done
• man less	- Done
• man more	- Done

##Task 2a
• ln test1.txt test2.txt 	- creates a hard link between test2.txt and test1.txt; theese files are mirroring each other and when you delete one of them the
								information in another will not be lost
• ln -s test2.txt test3.txt	- creates a soft link test3.txt that points text2.txt; when you delete test2.txt all information will be lost