class Cell:
    item = None
    next_item = None

    def __init__(self, item) -> None:
        self.item = item
        self.next_item = None

    def __next__(self):
        if self.next_item:
            return self.next_item

    def __str__(self):
        return self.item.__str__()

    def __get__(self, instance, owner):
        return self.item

    def has_next(self):
        return self.next_item

    def _add_cell_(self, item):
        self.next_item = Cell(item)

    def _next_str_(self):
        if self.has_next():
            return self.__str__() + ', ' + self.__next__()._next_str_()
        else:
            return self.__str__()

# ---------------------------------------------------------------------------------------------------------


class CustomList:
    head: Cell

    def __init__(self, items_list=None):
        self.head = None
        if items_list:
            for element in items_list:
                self.append(Cell(element))

    def __iter__(self) -> Cell:
        cell = self.head
        while cell is not None:
            yield cell
            cell = next(cell)

    def __str__(self):
        if self.head:
            return '[' + self.head._next_str_() + ']'
        return '[]'

    def __next__(self):
        if self.head:
            return self.head
        else:
            raise StopIteration

    def __len__(self) -> int:
        if self.head is None:
            return 0
        for i, element in enumerate(self, 1):
            if not element.has_next():
                return i
        return None

    def __getitem__(self, index) -> Cell:
        if self.__check_index__(index):
            for i, element in enumerate(self, 0):
                if i == index:
                    return element
        return None

    def __setitem__(self, index, item):
       if self.__check_index__(index):
            for i, element in enumerate(self, 0):
                if i == index:
                    element.item = item

    def __contains__(self, item) -> bool:
        return self._get_index_(item) is not None

    def __check_index__(self, index) -> bool:
        if not isinstance(index, int):
            raise TypeError(f'Wrong type of index: {type(index).__name__} = {index}')
        elif index < 0 or index >= self.__len__():
            raise IndexError(f'Index out of boundaries: {index}')
        else:
            return True

    def _append_to_tale_(self, item):
        self._get_tale_()._add_cell_(item)

    def _get_tale_(self):
        tale = self.head
        while tale.has_next():
            tale = next(tale)
        return tale

    def _get_index_(self, item) -> int:
        for i, element in enumerate(self, 0):
            if item == element.item:
                return i
        return None

    def append(self, item):
        if self.is_empty():
            self.head = Cell(item)
        else:
            self._append_to_tale_(item)

    def is_empty(self):
        return self.head is None

    def get(self, index) -> Cell:
        if self.__check_index__(index):
            return self.__getitem__(index)
        return None

    def pop(self, index):
        deleted = None
        if self.__check_index__(index):
            if index == 0:
                deleted = self[index]
                self.head = next(deleted)
            else:
                element = self[index-1]
                deleted = next(element)
                element.next_item = next(next(element))
        return deleted

    def insert(self, index, item):
        if self.__check_index__(index):
            if index == 0:
                inserted = Cell(self.head.item)
                inserted.next_item = next(self.head)
                self.head.next_item = inserted
                self.head.item = item
            else:
                inserted = Cell(item)
                element = self[index-1]
                inserted.next_item = next(element)
                element.next_item = inserted

    def replace(self, index, item):
        if self.__check_index__(index):
            self.__setitem__(index, item)

    def get_index(self, item):
        return self._get_index_(item)

    def clear(self):
        self.head = None


if __name__ == '__main__':
    my_list = CustomList()              # CREATING EMPTY LIST
    my_list.append((1, 2, 3))           # APPENDING TUPLE INTO My LIST
    my_list.append([1, 2, 3])           # APPENDING LIST INTO My LIST
    my_list.append({'a': 1, 'b': 2})    # APPENDING DICT INTO My LIST
    my_list.append(256)                 # APPENDING NUMBER INTO My LIST
    my_list.append('Andrii Chornyi')    # APPENDING STRING INTO My LIST
    print(my_list)                      # PRINTING THE WHOLE LIST TO CONSOLE
    print('>>> print(next(my_list.get(3)))')
    print(next(my_list.get(3)))         # CHECKING IF MY LIST IS ITERABLE && GET BY INDEX METHOD
    print('deleted using pop(2): ', my_list.pop(2))  # CHECKING pop METHOD
    print(f'>>> my_list.insert(2, "it was a dict")')
    print(f'>>> my_list.replace(3, 512)')
    my_list.insert(2, 'it was a dict')      # CHECKING insert METHOD
    my_list.replace(3, 512)                 # CHECKING replace METHOD
    print(my_list)
    print(f'>>> my_list[0] = (1, 2, 3, 4, 5)')
    print(f'>>> print(my_list[0])')
    my_list[0] = (1, 2, 3, 4, 5)            # CHECKING [index] ACCESS - CHANGING [0] ELEMENT
    print(my_list[0])                       # CHECKING [index] ACCESS - PRINTING [0] ELEMENT
    print('List length:', len(my_list))     # CHECKING len METHOD
    print('--------FOR---------for element in my_list: print(element)')
    for element in my_list:                 # CHECKING foreach INTEGRATION
        print(element)
    print('--------IF----------')
    find_element = 'it was a dict'          # CHECKING if contains INTEGRATION && GET INDEX BY VALUE
    if find_element in my_list:
        print(f'index of element "{find_element}" is: ', my_list.get_index(find_element))
    print('-------CLEAR--------')
    my_list.clear()
    print('Now my_list is empty: ', my_list)                # CHECKING CLEAR METHOD
    my_new_list = CustomList(['L', 'I', 'S', 'T'])          # CHECKING CREATING LIST OF EXISTING LIST
    print('List created of LIST: ', my_new_list)
    my_list = CustomList({'S', 'E', 'T'})                   # CHECKING CREATING LIST OF EXISTING SET
    print('List created of SET: ', my_list)
    my_tuple_list = CustomList(('T', 'U', 'P', 'L', 'E'))   # CHECKING CREATING LIST OF EXISTING TUPLE
    print('List created of TUPLE: ', my_tuple_list)

    list_of_none = CustomList(None)                         # CHECKING CREATING LIST OF NONE
    list_of_none.append(None)
    list_of_none.insert(0, None)
    print('append(None) than insert(0, None): ', list_of_none)
                                                            # CHECKING CREATING LIST OF OTHER CUSTOM LIST
    list_of_my_list = CustomList(my_list)
    print('List created of other CustomList: ', list_of_my_list)
                                                            # CHECKING APPENDING OTHER CUSTOM LIST AS AN ELEMENT
    list_of_my_list.append(my_list)
    print('Appending other CustomList as an element: ', list_of_my_list)
                                                            # CHECKING INSERTING OTHER CUSTOM LIST AS AN ELEMENT
    list_of_my_list.insert(1, my_list)
    print('Inserting other CustomList as an element: ', list_of_my_list)