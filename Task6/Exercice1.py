def combine_dicts(*args):

    def join_to_common_kv_list(*args: dict) -> list:
        common_kv_list = []
        for i in range(len(args)):
            common_kv_list.extend(list(args[i].items()))
        return common_kv_list

    def group_by_key(kv_list: list) -> list:
        return [(k, sum([v for _k, v in kv_list if _k == k])) for k in dict(kv_list).keys()]

    return dict(group_by_key(join_to_common_kv_list(*args)))


if __name__ == '__main__':
    dict1 = {'a': 100, 'b': 200}
    dict2 = {'a': 200, 'c': 300}
    dict3 = {'a': 300, 'd': 100}
    print(combine_dicts(dict1, dict2, dict3))
