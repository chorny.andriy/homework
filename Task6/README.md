# Task 6

Task 6 - Done.

Exercise 1 - implemented joining all dicts into one list of paired tuples, grouping by keys (k, sum(v)) 
and than converting list back to dict

Exercise 2 - implemented two clesses:
class Cell:
    item = None
    next_item = None

    def __init__(self, item) -> None:
    def __next__(self):
    def __str__(self):
    def __get__(self, instance, owner):
    def has_next(self):
    def _add_cell_(self, item):
    def _next_str_(self):

class CustomList:
    head: Cell

    def __init__(self, items_list=None):
    def __iter__(self) -> Cell:
    def __str__(self):
    def __next__(self):
    def __len__(self) -> int:
    def __getitem__(self, index) -> Cell:
    def __setitem__(self, index, item):
    def __contains__(self, item) -> bool:
    def __check_index__(self, index) -> bool:
    def _append_to_tale_(self, item):
    def _get_tale_(self):
    def _get_index_(self, item) -> int:
    def append(self, item):
    def is_empty(self):
    def get(self, index) -> Cell:
    def pop(self, index):
    def insert(self, index, item):
    def replace(self, index, item):
    def get_index(self, item):
    def clear(self):


