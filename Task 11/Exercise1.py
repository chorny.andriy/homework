from functools import wraps
a = "I am global variable!"


def enclosing_function_decorator(func = None):
    a = "I am variable from enclosed function!"

    @wraps(func)
    def inner_function():
        a = "I am local variable!" # This is inner_function local variable
        # global a # will print a from global scope (Task 1 - 2.1.)
        # nonlocal a # will print a from enclosing_function_decorator scope (Task 1 - 2.2.)
        print(a)
    return inner_function

@enclosing_function_decorator
def call_inner_from_enclosing():
    pass



if __name__ == '__main__':
    call_inner_from_enclosing()

