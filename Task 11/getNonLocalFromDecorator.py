from functools import wraps
a = "I am global variable!"


def enclosing_function_decorator(func = None):
    a = "I am variable from enclosed function!"
    enclosing_function_decorator.__setattr__('a', a)

    @wraps(func)
    def inner_function():
        a = func() or "I am local variable!"
        print(a)
    return inner_function

@enclosing_function_decorator
def call_inner_from_enclosing():
    pass

@enclosing_function_decorator
def call_inner_from_enclosing_set_a_global():
    global a
    return a

@enclosing_function_decorator
def call_inner_from_enclosing_set_a_nonlocal():
    return enclosing_function_decorator.a   # Just a hardcode till better decision is found.
    # print(call_inner_from_enclosing_set_a_nonlocal.a)
    # nonlocal a
    # return enclosing_function_decorator.__getattribute__('a')


if __name__ == '__main__':
    call_inner_from_enclosing()
    call_inner_from_enclosing_set_a_global()
    call_inner_from_enclosing_set_a_nonlocal()


