def remember_result(func):
    last_result = None

    def inner(*args):
        nonlocal last_result
        print(f'Last result = {last_result}')
        last_result = func(*args)
        return last_result
    return inner


@remember_result
def sum_list(*args):
    result = ""
    for item in args:
        result += str(item)
    print(f"Current result = '{result}'")
    return result


sum_list('1', '2')
sum_list('3', '4')
sum_list('5', '6')
