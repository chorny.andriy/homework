from decimal import *
import inspect


class Employee:
    __name: str
    __salary: Decimal
    _bonus: Decimal

    def __init__(self, name: str, salary: Decimal):
        self.__name = name
        self.salary = salary
        self._bonus = None

    def __str__(self):
        return self.__class__.__name__ + ': ' + self.name

    @staticmethod
    def __is_money_type(money):
        if not isinstance(money, (Decimal, int)):
            raise TypeError(f'Wrong type of salary: {type(money).__name__}')
        return True

    @property
    def name(self):
        return self.__name

    @property
    def salary(self):
        return self.__salary

    @salary.setter
    def salary(self, salary):
        if self.__is_money_type(salary):
            self.__salary = salary

    def set_bonus(self, bonus):
        if self.__is_money_type(bonus):
            self._bonus = bonus

    @property
    def to_pay(self):
        return self.__salary + self._bonus


class SalesPerson(Employee):
    __percent: int

    def __init__(self, name: str, salary: Decimal, percent: int):
        super(SalesPerson, self).__init__(name, salary)
        if self.__is_percentage_type__(percent):
            self.__percent = percent

    @staticmethod
    def __is_percentage_type__(percent):
        if not isinstance(percent, int) and percent < 0:
            raise TypeError(f'Wrong type of percents: {type(percent).__name__}')
        return True

    def set_bonus(self, bonus: Decimal):
        multiplier = 1
        if self.__percent > 100:
            multiplier = 2
        if self.__percent > 200:
            multiplier = 3
        self._bonus = bonus * multiplier


class Manager(Employee):
    __quantity: int

    def __init__(self, name: str, salary: Decimal, client_amount: int):
        super(Manager, self).__init__(name, salary)
        self.__quantity = client_amount

    def set_bonus(self, bonus):
        increase = 500 if self.__quantity > 100 else 0
        increase = 1000 if self.__quantity > 150 else increase
        self._bonus = bonus + increase


class Company:
    __employees: list

    def __init__(self, *employees: Employee):
        self.__employees = []
        for employee in employees:
            self.add_employee(employee)

    def __getitem__(self, item):
        return self.__employees.__getitem__(item)

    def __str__(self):
        return str([employee.__str__() for employee in self])

    def add_employee(self, employee):
        if Employee.__name__ not in [mro_class.__name__ for mro_class in inspect.getmro(employee.__class__)]:
            raise TypeError(f'Type of staff should be from Employee class stack, not: {employee.__class__.__name__}')
        self.__employees.append(employee)

    def give_everybody_bonus(self, company_bonus):
        for employee in self:
            employee.set_bonus(company_bonus)

    def total_to_pay(self):
        return sum([employee.to_pay for employee in self])

    def name_max_salary(self):
        return max([(employee.to_pay, employee.name) for employee in self])[1]


if __name__ == '__main__':
    print('-----------------------------------------------------------------------')
    print('Initializing SalesPerson Andriy: salary - 2000, plan performance - 201%')
    sales_person = SalesPerson('Andriy', 2000, 201)
    print(sales_person, f'- $ {sales_person.salary}')
    print('-----------------------------------------------------------------------')
    print('Changing salary to $3000 using @property method:')
    sales_person.salary = 3000
    print(sales_person, f'- $ {sales_person.salary}')
    print('-----------------------------------------------------------------------')
    print('Setting bonus - 178.50')
    sales_person.set_bonus(178.50)
    print('-----------------------------------------------------------------------')
    print('Calculating ToPay: ')
    print(sales_person, f'to pay: {sales_person.to_pay}')
    print('-----------------------------------------------------------------------')
    print('Initializing Manager Vasyl: salary - 2000, quantity of served clients - 101')
    manager = Manager('Vasyl', 2000, 101)
    print(f'{manager} - ${manager.salary}')
    print('-----------------------------------------------------------------------')
    print('Setting bonus - 218.5')
    manager.set_bonus(218.5)
    print('-----------------------------------------------------------------------')
    print('Calculating ToPay: ')
    print(manager, f'to pay: {manager.to_pay}')
    print('-------------------------------COMPANY---------------------------------')
    company = Company(sales_person, manager)
    print(f'Company: {company}')
    print('-----------------------------------------------------------------------')
    print('Trying to add int object, instead of Employee, to a Company')
    try:
        company.add_employee(156)
    except TypeError as type_error:
        print(f'Caught TypeError exception: {type_error}')
    print('-----------------------------------------------------------------------')
    print('Lets give everybody in our Company $ 300 bonus')
    company.give_everybody_bonus(300)
    print(f'Total to pay: {company.total_to_pay()}')
    print('-----------------------------------------------------------------------')
    print(f'Employee with max. salary, including bonus: {company.name_max_salary()}')