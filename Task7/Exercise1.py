
class Rectangle:
    __side = (float, float)

    def __init__(self, a: float, b: float = 5):
        if self.__side_is_valid__(a) and self.__side_is_valid__(b):
            self.__side = (a, b)

    def __str__(self):
        return f'{self.side_a} x {self.side_b}'

    @staticmethod
    def __side_is_valid__(side):
        if not isinstance(side, (float, int)):
            raise TypeError(f'Side of rectangle has wrong type: {type(side).__name__}')
        if side is None or side <= 0:
            raise ValueError(f'Side of rectangle is undefined: {side.__name__}')
        return True

    @property
    def side_a(self) -> float:
        if self.__side_is_valid__(self.__side[0]):
            return self.__side[0]

    @property
    def side_b(self) -> float:
        if self.__side_is_valid__(self.__side[1]):
            return self.__side[1]

    @property
    def area(self):
        return self.side_a * self.side_b

    @property
    def perimeter(self):
        return 2 * (self.side_a + self.side_b)

    @property
    def is_square(self):
        return self.side_a == self.side_b

    def replace_sides(self):
        self.__side = (self.side_b, self.side_a)


class ArrayRectangles:
    __rectangle_array: list
    __max_length: int

    def __init__(self, array_length, *rectangles):
        self.__rectangle_array = []
        self.__max_length = array_length
        if not self.add_rectangle(*rectangles):
            raise ValueError(f'Trying to initialize too large array: max_len = {self.__max_length}')

    def __getitem__(self, item):
        return self.__rectangle_array.__getitem__(item)

    def __len__(self):
        return self.__rectangle_array.__len__()

    def __str__(self):
        return str([str(rectangle) for rectangle in self])

    @property
    def max_length(self):
        return self.__max_length

    def check_valid_argument(self, rectangle):
        if not isinstance(rectangle, (Rectangle, ArrayRectangles)):
            raise TypeError(f'Type of *rectangles should be Rectangle or ArrayRectangles. '
                            f'But given: {rectangle.__class__.__name__}')

    def check_free_memory_for_rectangles_list(self, rectangle):
        if len(self) > self.__max_length - len(rectangle):
            raise ValueError('Given list of rectangles is to large to contain into rectangles array')

    def check_free_memory_for_rectangle(self):
        if len(self) == self.__max_length:
            raise ValueError('Rectangles array is full and cannot be appended')

    def add_rectangle(self, *rectangles):
        for rectangle in rectangles:
            try:
                self.check_valid_argument(rectangle)
                try:
                    self.check_free_memory_for_rectangles_list(rectangle)
                    self.__rectangle_array.extend(rectangle)
                except TypeError:
                    self.check_free_memory_for_rectangle()
                    self.__rectangle_array.append(rectangle)
            except ValueError:
                return False
        return True

    def get_index_of_max_area(self):
        return max((rectangle.area, index) for index, rectangle in enumerate(self, 0))[1]

    def get_index_of_min_perimeter(self):
        return min((rectangle.perimeter, index) for index, rectangle in enumerate(self, 0))[1]

    def get_number_of_squares(self):
        return [rectangle.is_square for rectangle in self].count(True)


if __name__ == '__main__':
    print('---------------------------------------------------------------------------------------------------')
    array_length = 5
    print(f'Setting up max_length of ArrayRectangles: {array_length}')
    print('---------------------------------------------------------------------------------------------------')
    add_rectangle_list = ArrayRectangles(2, Rectangle(20), Rectangle(25, 25))
    print(f'Creating a list of rectangles: {add_rectangle_list}, max_length: {add_rectangle_list.max_length}')
    print('---------------------------------------------------------------------------------------------------')
    rectangles = ArrayRectangles(array_length, Rectangle(10), Rectangle(15), add_rectangle_list)
    print(f'Creating new list o rectangles: ({Rectangle(10)}, {Rectangle(15)}, {add_rectangle_list})'
          f'\nof two rectangles (10 x 5, 15 x 5) and a list of two rectangles [20 x 5, 25 x 25]')
    print('---------------------------------------------------------------------------------------------------')
    invalid_object = 125
    print(f'Trying to add invalid object {type(invalid_object).__name__}: {invalid_object} to ArrayRectangles')
    try:
        rectangles.add_rectangle(invalid_object)
    except TypeError as type_error:
        print(f'Caught TypeError: {type_error}')
    print('---------------------------------------------------------------------------------------------------')
    figure = Rectangle(5.5)
    print(f'Adding new rectangle to ArrayRectangles: {figure}')
    if rectangles.add_rectangle(figure):
        print(f'{figure} successfully added as a {len(rectangles)}-th element to ArrayRectangles')
    print('---------------------------------------------------------------------------------------------------')
    figure = Rectangle(18.5, 18.5)
    print(f'Adding new rectangle to ArrayRectangles: {figure}')
    if not rectangles.add_rectangle(figure):
        print(f'Rectangles array is full. Cannot append {len(rectangles)+1}-th element to ArrayRectangles')
    print('---------------------------------------------------------------------------------------------------')
    print('----------------------------------RECTANGLES IN ArrayRctangles-------------------------------------')
    for i, rectangle in enumerate(rectangles, 0):
        print(f'\nRectangle {i}:   ', rectangle)
        print(f'                square:    ', rectangle.area)
        print(f'                perimeter: ', rectangle.perimeter)
        print(f'                is square: ', rectangle.is_square)
        rectangle.replace_sides()
        print(f'           replaced sides: ', rectangle)
    print('---------------------------------------------------------------------------------------------------')
    print('---------------------------------------------------------------------------------------------------')
    print(f'The index of a rectangle with max area is: ', rectangles.get_index_of_max_area())
    print(f'The index of a rectangle with min perimeter is: ', rectangles.get_index_of_min_perimeter())
    print(f'The number of squares in rectangles array is: ', rectangles.get_number_of_squares())
