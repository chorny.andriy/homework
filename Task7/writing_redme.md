# Task 7

## Exercise 1  - Advanced level - Done

Class Rectangle - class contains private tuple __side of two elements - sedes lengths

###Low level:

Constructor __init__() receives one or two sides lengths (float) and, if only one side is given, specifies side A of a rectangle, 
side B is always equal to 5, in other case constructor specifies both sides by given values.

Method __str__() is overridden to return string in format "A x B", where A, B - sides lengths of rectangle.

Static method __side_is_valid__() checks if side is of proper type and value, raises TypeError or ValueError, or returns True

Property **side_a** returns length of side A

Property **side_b** returns length of side B

Property **area returns** an area of the rectangle

Property **perimeter** returns a perimeter of the rectangle

Property **is_square** returns True if the rectangle is a square

Method **replace_sides** swaps rectangles sides

###Advanced level:



#### Task 1  
Develop **Rectangle** and **ArrayRectangles** with a predefined functionality.  
On a **Low** level it is obligatory:  
To develop **Rectangle** class with following content:  
2 closed float **sideA** and **sideB** (sides A and B of the rectangle).  
Constructor with two parameters **a** and **b** (parameters specify rectangle 
sides).  
Constructor with a parameter **a** (parameter specify side A of a rectangle, 
side B is always equal to 5).  
Method **GetSideA**, returning value of the side A.  
Method **GetSideB**, returning value of the side B.  
Method **Area**, calculating and returning the area value.  
Method **Perimeter**, calculating and returning the perimeter value.  
Method **IsSquare**, checking whether current rectangle is shape square or not. 
Returns True if the shape is square and False in another case.  
Method **ReplaceSides**, swapping rectangle sides.  
On **Adwanced** level also needed:  
Complete level **Low** Assignment.  
Develop class **ArrayRectangles**, in which declare:  
Private field **rectangle_array** - array of rectangles.  
Constructor creating an empty array of rectangles with length **n**.  
Constructor that receives an arbitrary ammount of objects of type **Rectangle** 
or an array of objects of type **Rectangle**.  
Method **AddRectangle** that adds a rectangle of type **Rectangle** to the 
array on the nearest free place and returning True, or returning False, if 
there is no free space in the array.  
Method **NumberMaxArea**, that returns order number (index) of the rectangle 
with the maximum area value (numeration starts from zero).  
Method **NumberMinPerimeter**, that returns order number (index) of the 
rectangle with the minimum perimeter value (numeration starts from zero).  
Method **NumberSquare**, that returns the number of squares in the array of 
rectangles.

#### Task 2  
To create classes **Employee, SalesPerson, Manager** and **Company** with 
predefined functionality.  
**Low** level requires:  
To create basic class **Employee** and declare following contents:  
Three closed fields - text field **name** (employee last name), money fields - 
**salary** and **bonus**.  
Public property **Name** for reading employee's last name.  
Public property **Salary** for reading and recording salary field.  
Constructor with parameters string **name** and money **salary** (last name 
and salary are set).  
Method **SetBonus** that set bonuses to salary, amount of which is 
delegated/conveyed as bonus.  
Method **ToPay** that returns the value of summarized salary and bonus.  
To create class **SalesPerson** as class **Employee** inheritor and declare 
within it:  
Closed integer field **percent** (percent of sales targets plan 
performance/execution).  
Constructor with parameters: **name** - employee last name, **salary, 
percent** - percent of plan performance, first two of which are passed to basic 
class constructor.  
Redefine method pf parent class **SetBonus** in the following way: if the 
salesperson completed the plan more than 100%, so his bonus is doubled (is 
multiplied by 2), and if more than 200% - bonus is tripled (is multiplied by 
3).  
To create class **Manager** as **Employee** class inheritor, and declare with 
it:  
Closed integer field **quantity** (number of clients, who were served by the 
manager during the month).  
Constructor with parameters string **name** - employee last name, **salary** 
and integer **clientAmount** - number of served clients, first two of which 
are passed to basic class constructor.  
Redefine method of parent class **SetBonus** in the following way: if the 
manager served over 100 clients, his bonus is increased by 500, and if more 
than 150 - by 1000.  
**Advance** level requires:  
To fully complete **Low** level tasks.  
Create class **Company** and declare within it:  
Constructor that receives employee array of **Employee** type with arbitrary 
length.  
Method **GiveEverybodyBonus** with money parameter **companyBonus** that sets 
the amount of basic bonus for each employee.  
Method **TotalToPay** that returns total amount of salary of the employees 
including awarded bonus.  
Method **NameMaxSalary** that returns employee last name, who received maximum 
salary including bonus.