def swap_words(sentence):
    lst = sentence.split(' ')
    try:
        a = lst.index('reasonable')
        b = lst.index('unreasonable')
        lst[a], lst[b] = lst[b], lst[a]
        return ' '.join(lst).strip()
    except ValueError:
        return ''

def swap_words_slice_crutch(sentence : str):
    a = 'reasonable'
    b = 'unreasonable'
    try:
        return sentence[0 : sentence.index(a)] + \
           b + \
           sentence[sentence.index(a) + len(a) : sentence.index(b)] + \
           a + \
           sentence[sentence.index(b) + len(b) :]
    except ValueError:
        return ''


if __name__ == '__main__':
    sentence = 'The reasonable man adopts himself to the world; ' \
               'the unreasonable one persists in trying to adopt the world to himself'
    print(f'Original sentence:\n{sentence}\n\nSwapped sentence:')
    print(swap_words(sentence))
    print(swap_words_slice_crutch(sentence))