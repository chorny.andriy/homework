def count_negatives(numbers_list) -> int:
#    return list(map(lambda x: x + abs(x), numbers_list)).count(0)
#    return ''.join(map(str, numbers_list)).count('-')
    return str(numbers_list).count('-')


if __name__ == '__main__':
    input_numbers = [4, -9, 8, -11, 8]
    print(f'There are {count_negatives(input_numbers)} negative numbers in list {input_numbers}')

