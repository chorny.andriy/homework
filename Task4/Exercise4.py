
if __name__ == '__main__':
    number = int(input('Input number: '))
    sum = 0
    binary_str = ''

# ------------------------RECURSIVE---------------------------------------------------------------------

    def convert_to_bin(number):
        if number < 0:
            print('Number cannot be negative')
            return None
        return (str(convert_to_bin(number // 2)) if (number > 1) else '') + str(number % 2)

    def count_ones(binary_str):
        return int(binary_str[0]) + count_ones(binary_str[1:]) if len(binary_str) >= 1 else 0

    bin_str = convert_to_bin(number)
    print(f'Number is {number}, binary representation is {bin_str}, sum is {count_ones(bin_str)}')

#------------------------LOOP--------------------------------------------------------------------------
    i = number
    while (i >= 1):
        binary = i % 2
        sum += binary
        binary_str = str(binary) + binary_str
        i //= 2

    print(f'Number is {number}, binary representation is {binary_str}, sum is {sum}')
