matrix = [[0, 1, 9], [9, 8, 5], [7, 3, 1], [3, 4, 0]]
result = []

def print_matrix(matrix: list):
    for i in matrix:
        print(i)

if __name__ == '__main__':

    result = [[matrix[j][i] for j in range(len(matrix))] for i in range(len(matrix[0]))]

    print('Original matrix is:')
    print_matrix(matrix)
    print('\nTransposed matrix is:')
    print_matrix(result)
