class Fibo():
    fibb = None

    def __init__(self, length):
        if (length < 2):
            print('You\'ve entered too short length of Fibonacci sequence')
        else:
            self.get(length)

    def __str__(self):
        return self.fibb.__str__()

    def get(self, length):
        if (not self.fibb):
            self.fibb = [0, 1]
        if len(self.fibb) >= length:
            return self.fibb
        else:
            self.fibb.append(self.fibb[len(self.fibb) - 1] + self.fibb[len(self.fibb) - 2])
            return self.get(length)

    def sum(self):
        return sum(self.fibb) if self.fibb else None
#----------------------------------------------------------------------------------------------

if __name__ == '__main__':
    length = int(input('Please enter the length of sequence (must be > 2): '))

#-----------------------RECURSION-VIA-CLASS-----------------------------------------------------

    fib = Fibo(length)
    print(fib)
    print(f'Sum of elements in Fibonacci sequence is: {fib.sum()}')

#-----------------------MIXED-RECURSION+LOOP-VIA-FINCTION--------------------------------------------------

    def fibo_func(length):
        if length <= 1:
            return length
        else:
            return fibo_func(length-1) + fibo_func(length-2)

    if (length <0):
        print('Length of Fibonacci sequence cannot be negative')
    else:
        fib_sequence = []
        fib_sum = 0
        for i in range(length):
            fib_sequence.append(fibo_func(i))
            fib_sum += fibo_func(i)
        print(fib_sequence)
        print(f'Sum of elements in Fibonacci sequence is: {fib_sum}')

#------------------------LOOP-------------------------------------------------------------------

    fib = [0, 1]
    if (length >= 2):
        while (len(fib) < length):
            fib.append(fib[len(fib)-1] + fib[len(fib)-2])
        print(fib)
        print(f'Sum of elements in Fibonacci sequence is: {sum(fib)}')
    else:
        print('You\'ve entered too short length of Fibonacci sequence' )

#-------------------------RECURSIVE FUNCTION ONLY----------------------------------------

    def f(l, fib=None):
        if 0 <= l <= 1:
            return [0]
        if not fib:
            fib = [0, 1]
        if len(fib) >= l:
            return fib
        else:
            fib.append(fib[len(fib) - 1] + fib[len(fib) - 2])
            return f(l, fib)

    print(f(length))
    print(f'Sum of elements in Fibonacci sequence is: {sum(f(length))}')

