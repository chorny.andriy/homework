
if __name__ == '__main__':
    number = int(input('Enter a positive number: '))

    #--------------------RECURSION-------------------------------------

    def fact(n):
        return 1 if (n == 1 or n == 0) else n * fact(n-1)

    if (number >= 0):
        print(f'{number}! = ', fact(number))
    else:
        print(f'Cannot calculate factorial for NEGATIVE number')

    #-----------------------LOOP-----------------------------------

    result = 1
    if (number >=0 ):
        for i in range(1, number+1):
            result *= i
        print(f'{number}! = ', result)
    else:
        print(f'Cannot calculate factorial for NEGATIVE number')
