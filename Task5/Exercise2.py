# -----------------------------------CLASS DECISION------------------------------------------
from homework.homework.Task5.Exercise1 import Array, ASC, DESC


class TransformArray(Array):
    def transform(self, start=0):
        self.values[start] += start
        if not start == len(self.values)-1:
            self.transform(start+1)
        else:
            return

    def transform_if_sorted(self, sort_order):
        if self.is_sorted(sort_order):
            self.transform()

# -------------------------------FUNCTION DECISION------------------------------------------------------------


from homework.homework.Task5.Exercise1 import is_sorted


def transform(array, sort_order, start=0):
    array[start] += start
    if not start == len(array) - 1:
        transform(array, sort_order, start + 1)
    return array


def transform_if_sorted(array, sort_order):
    if is_sorted(array, sort_order):
        return transform(array, sort_order)
    return array


# ------------------------------------------------------------------------------------------------------------
if __name__ == '__main__':
    print('Class implementation')
    print('Ascending array')
    arr = TransformArray([1, 2, 3, 4, 5])
    print(arr.values)
    arr.transform_if_sorted(ASC)
    print(arr.values)
    arr.transform_if_sorted(DESC)
    print(arr.values)
    print('Descending array')
    arr = TransformArray([5, 4, 3, 2, 1])
    print(arr.values)
    arr.transform_if_sorted(ASC)
    print(arr.values)
    arr.transform_if_sorted(DESC)
    print(arr.values)

    print('Function implementyation')
    print(transform_if_sorted([1, 2, 3, 4, 5], ASC))
    print(transform_if_sorted([1, 2, 3, 4, 5], DESC))
    print(transform_if_sorted([5, 4, 3, 2, 1], ASC))
    print(transform_if_sorted([5, 4, 3, 2, 1], DESC))
