def sum_geometric_elements(a1, multiplier, alim):
    a_next = a1 * multiplier
    if a_next > alim:
        return a1 + sum_geometric_elements(a_next, multiplier, alim - 1)
    else:
        return a1


if __name__ == '__main__':
    print(sum_geometric_elements(100, 0.5, 20))
