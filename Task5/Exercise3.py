def mult_arithmetic_elements(a1: int, step: int, seq_length: int):
    if seq_length > 1:
        return a1 * mult_arithmetic_elements(a1 + step, step, seq_length - 1)
    else:
        return a1


if __name__ == '__main__':
    print(mult_arithmetic_elements(5, 3, 4))
