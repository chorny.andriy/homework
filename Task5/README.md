# Task 5

Task 5 - Done.
Exercise 1 - checking if Array is sorted - implemented in two ways (classes, simple functions) using recursion 
Exercise 2 - Array transformation is sorted - implemented in two ways (classes, simple functions) using recursion
Exercise 3 - Multiplication of arithmetical progression - implemented in one function using recursion
Exercise 4 - Sum of decreasing geometric progression - implemented in one function using recursion
