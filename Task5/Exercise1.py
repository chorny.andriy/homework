ASC = asc = 'asc'
DESC = desc = 'desc'

# ------------------------------CLASS DECISION-----------------------------------------


class Array:
    values = []

    def __init__(self, val):
        self.values = val

    @staticmethod
    def asc(a, b):
        return a <= b

    @staticmethod
    def desc(a, b):
        return a >= b

    def is_sorted(self, order_attr: str, start=0) -> bool:
        if start == len(self.values)-1:
            return True
        if self.__getattribute__(order_attr.lower())(self.values[start], self.values[start+1]):
            return self.is_sorted(order_attr, start+1)
        else:
            return False

# --------------------------------FUNCTION DECISION-------------------------------------


def is_sorted(array, order_attr, start=0) -> bool:
    if start == len(array) - 1:
        return True
    if (array[start] <= array[start + 1] and order_attr.lower() == 'asc')\
            or (array[start] >= array[start + 1] and order_attr.lower() == 'desc'):
        return is_sorted(array, order_attr, start + 1)
    else:
        return False

# --------------------------------------------------------------------------------------


if __name__ == '__main__':
    print('Class call:')
    arr = Array([1, 2, 3, 4, 5])
    print(arr.values, ' asc : ', arr.is_sorted(asc))
    print(arr.values, ' desc : ', arr.is_sorted(desc))
    arr = Array([5, 4, 3, 2, 1])
    print(arr.values, ' asc : ', arr.is_sorted(asc))
    print(arr.values, ' desc : ', arr.is_sorted(desc))
    arr = Array([5, 4, 30, 2, 1])
    print(arr.values, ' asc : ', arr.is_sorted(asc))
    print(arr.values, ' desc : ', arr.is_sorted(desc))
    print('Function call:')
    print('[1, 2, 3, 4, 5] asc : ', is_sorted([1, 2, 3, 4, 5], asc))
    print('[1, 2, 3, 4, 5] desc : ', is_sorted([1, 2, 3, 4, 5], desc))
    print('[5, 4, 3, 2, 1] asc : ', is_sorted([5, 4, 3, 2, 1], asc))
    print('[5, 4, 3, 2, 1] desc : ', is_sorted([5, 4, 3, 2, 1], desc))
    print('[1, 2, 3, 40, 5] asc : ', is_sorted([1, 2, 3, 40, 5], asc))
    print('[1, 2, 3, 40, 5] desc : ', is_sorted([1, 2, 3, 40, 5], desc))
