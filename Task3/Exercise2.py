if __name__ == '__main__':
    number = int(input('Enter thr number: '))

    # ---------------------------------------------------------------------------------------
    message = (number % 3 == 0) * 'Fizz' + (number % 5 == 0) * 'Buzz'
    message += (not message.isalpha()) * str(number)
    print((str(number) + ' is out of range [1, 100]', message)[number in range(1, 100)])

    # ----------------------------------------------------------------------------------------
    message = ''
    if number in range(1, 100):
        if number % 3 == 0:
            message += 'Fizz'
        if number % 5 == 0:
            message += 'Buzz'
        if len(message) == 0:
            message += str(number)
        print(message)
    else:
        print(str(number) + ' is out of range [1, 100]')
