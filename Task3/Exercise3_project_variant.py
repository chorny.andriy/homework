cards_options = {
    'names' : {'2', '3', '4', '5', '6', '7', '8', '9', '10', 'Jack', 'Queen', 'King', 'Ace', 'Joker'},
    'suits' : {'Heart', 'Club', 'Diamond', 'Spades'}
}

class Card():
    card = (str, str)
    def __init__(self, card_name, card_suit):
        self.card = (card_name, card_suit)

    def __str__(self):
        return self.card.__str__()

class BaccaratCard(Card):
    def __init__(self):
        super.__init__()

    @property
    def card_score(self):
        return int(self.card[0]) if (self.card[0].isdigit() and len(self.card[0]) < 2) else \
            1 if str(self.card[0]).lower() == 'ace' else 0


class CardsBank:
    cards = set()

    def __str__(self):
        return str(element for element in self.cards)

    def add(self, card: Card):
        self.add(card)

    # def get(self, card_name):
    #     for name in self.cards[0]:
    #         return name

class BaccaratCardsBank(CardsBank):
    def __init__(self):
        for name in cards_options['names']:
            for suit in cards_options['suits']:
                self.cards.add(Card(name, suit))


class player_cards_pool(CardsBank):
    pass


if __name__ == '__main__':
    start_cards_quantity = 2;
    game_cards_bank = BaccaratCardsBank()
    player_cards_pool = CardsBank()

    # for i in range(start_cards_quantity):
        # player_cards_pool.add(Card(game_cards_bank.cards[]input(f'Play {i+1} card: ')))
    cb = Card('5', 'Club')

    print(cb)
    cb = BaccaratCardsBank(cb).add(cb)
    print(cb)
    cb = CardsBank()
    print(cb)
    # for baccarat_card in cb.cards.keys():
    #     print(baccarat_card, cb.cards.get(baccarat_card))
