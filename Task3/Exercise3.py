class Cards:
    cards = {'2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9, '10': 0,
             'J': 0, 'Q': 0, 'K': 0, 'A': 1, 'joker': None}


if __name__ == '__main__':
    card1 = Cards.cards.get(input('Play first card: '))
    card2 = Cards.cards.get(input('Play second card: '))

    result = 'Do not cheat' if None in (card1, card2) else (card1 + card2) % 10

    print(f'Your result is: {result}')
