import re
from collections import defaultdict
from operator import itemgetter
from collections import Counter


def most_common_words(file_path: str, common_words: int):
    """

    :param file_path:
    :param common_words:
    :return:
    """
    try:
        with open(file_path, 'r') as file:
            words_count = []
            words_count = defaultdict(lambda: 0, words_count)
            for word in re.findall(r"\b[a-zA-Z]+\b", file.read()):
                words_count[word] += 1
            return [word[0] for word in sorted(words_count.items(), key=itemgetter(1), reverse=True)[0:common_words]]
    except FileNotFoundError:
        print(fr'No such file or directory: {file_path}')

def most_common_words_using_counter(file_path: str, common_words: int):
    """
    
    :param file_path:
    :param common_words:
    :return:
    """
    try:
        with open(file_path, 'r') as file:
            words_count = Counter(re.findall(r"\b[a-zA-Z]+\b", file.read()))
            return [word[0] for word in words_count.most_common(common_words)]
    except FileNotFoundError:
        print(fr'No such file or directory: {file_path}')


if __name__ == '__main__':
    print(most_common_words('lorem_ipsum.txt', 7))
    print(most_common_words_using_counter('lorem_ipsum.txt', 7))

