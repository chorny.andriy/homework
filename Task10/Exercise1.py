if __name__ == '__main__':
    input_file_path = 'unsorted_names.txt'
    output_file_path = 'sorted_names.txt'
    try:
        with open(input_file_path, 'r') as f_unsorted:
            with open(output_file_path, 'w') as f_sorted:
                lst = f_unsorted.read().split('\n')
                lst.sort()
                f_sorted.write('\n'.join(lst))
    except FileNotFoundError:
        print(fr'No such file or directory: {input_file_path}')
