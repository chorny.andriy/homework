from operator import itemgetter


def get_top_performers(file_path: str, number_of_top_students: int):
    """

    :param file_path:
    :param number_of_top_students:
    :return:
    """
    try:
        with open(file_path, 'r') as students_file:
            students_file.readline()
            students_list = [(student.split(',')) for student in students_file.read().split('\n')]
            students_list = sorted(students_list, key=lambda x: float(x[2]), reverse=True)
            return [student[0] for student in students_list[0:number_of_top_students]]
    except FileNotFoundError:
        print(fr'No such file or directory: {file_path}')

def sort_by_age(file_path: str):
    """

    :param file_path:
    :param number_of_top_students:
    :return:
    """
    try:
        with open(file_path, 'r') as input_file:
            with open('sorted'+file_path, 'w') as output_file:
                csv_header = input_file.readline()
                students_list = [student.split(',') for student in input_file.read().split('\n')]
                students_list = sorted(students_list, key=lambda x: int(x[1]), reverse=True)
                output_file.write(csv_header)
                output_file.writelines([','.join(csv_line)+'\n' for csv_line in students_list])
    except FileNotFoundError:
        print(fr'No such file or directory: {input_file}')



if __name__ == '__main__':
    f = 'students.csv'
    print(get_top_performers(f, 5))
    sort_by_age(f)